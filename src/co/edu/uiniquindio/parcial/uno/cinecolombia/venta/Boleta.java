package co.edu.uiniquindio.parcial.uno.cinecolombia.venta;

import co.edu.uiniquindio.parcial.uno.cinecolombia.cine.Presentacion;
import co.edu.uiniquindio.parcial.uno.cinecolombia.cine.Sala;
import co.edu.uiniquindio.parcial.uno.cinecolombia.cine.Silla;
import co.edu.uiniquindio.parcial.uno.cinecolombia.hr.Cliente;
import co.edu.uiniquindio.parcial.uno.cinecolombia.hr.Taquillero;

public class Boleta {

    private Cliente cliente;
    private Taquillero taquillero;
    private Presentacion presentacion;
    private Silla silla;

    public Boleta(Cliente cliente, Taquillero taquillero, Presentacion presentacion, Silla silla) {
        this.cliente = cliente;
        this.taquillero = taquillero;
        this.presentacion = presentacion;
        this.silla = silla;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Taquillero getTaquillero() {
        return taquillero;
    }

    public Presentacion getPresentacion() {
        return presentacion;
    }

    public Silla getSilla() {
        return silla;
    }
}
