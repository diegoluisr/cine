package co.edu.uiniquindio.parcial.uno.cinecolombia.cine;

public enum TipoZona {
    PREFERENCIAL,
    NORMAL
}
