package co.edu.uiniquindio.parcial.uno.cinecolombia.cine;

import java.util.ArrayList;

public class Zona {

    private ArrayList<Silla> sillas;
    private TipoZona tipoZona;

    public Zona(TipoZona tipoZona) {
        this.tipoZona = tipoZona;
    }

    public ArrayList<Silla> getSillas() {
        return sillas;
    }
}
