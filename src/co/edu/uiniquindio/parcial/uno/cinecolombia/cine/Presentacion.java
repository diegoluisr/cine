package co.edu.uiniquindio.parcial.uno.cinecolombia.cine;

import java.util.Date;

public class Presentacion {
    private Pelicula pelicula;
    private Sala sala;
    private Date inicio;

    public Presentacion(Pelicula pelicula, Sala sala, Date inicio) {
        this.pelicula = pelicula;
        this.sala = sala;
        this.inicio = inicio;
    }

    public Pelicula getPelicula() {
        return pelicula;
    }

    public void setPelicula(Pelicula pelicula) {
        this.pelicula = pelicula;
    }

    public Sala getSala() {
        return sala;
    }

    public void setSala(Sala sala) {
        this.sala = sala;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }
}
