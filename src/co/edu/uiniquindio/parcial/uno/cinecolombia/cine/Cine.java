package co.edu.uiniquindio.parcial.uno.cinecolombia.cine;

import co.edu.uiniquindio.parcial.uno.cinecolombia.hr.Empleado;

import java.util.ArrayList;

public class Cine {
    private ArrayList<Sala> salas;
    private ArrayList<Empleado> empleados;

    public ArrayList<Sala> getSalas() {
        return salas;
    }

    public void setSalas(ArrayList<Sala> salas) {
        this.salas = salas;
    }

    public ArrayList<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(ArrayList<Empleado> empleados) {
        this.empleados = empleados;
    }
}
