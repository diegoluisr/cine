package co.edu.uiniquindio.parcial.uno.cinecolombia.cine;

public class Sala {

    private String name;
    private Zona[] zonas = new Zona[2];

    public Sala(String name) {
        this.name = name;
        zonas[0] = new Zona(TipoZona.NORMAL);
        zonas[1] = new Zona(TipoZona.PREFERENCIAL);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Zona[] getZonas() {
        return zonas;
    }

}
